from flask import Blueprint, jsonify, render_template, flash, g, abort, redirect, url_for

from module_events.models import Event

from ceredigionclp import app, db, bcrypt, login_manager, Map

events = Blueprint("events", __name__, url_prefix="/events")

@events.route("/", methods=["GET", "POST"])
def index():

    map = Map(
        identifier="catsmap",
        lat=37.4419,
        lng=-122.1419,
        style="height:95%;width:100%;max-height:500px;",
        markers=[
            {
                'icon': 'http://maps.google.com/mapfiles/ms/icons/green-dot.png',
                'lat':  37.4419,
                'lng':  -122.1419,
                'infobox': "B"
            },
            {
                'icon': 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png',
                'lat': 37.4300,
                'lng': -122.1400,
                'infobox': "B"
            },
            {
                'icon': 'http://maps.google.com/mapfiles/ms/icons/yellow-dot.png',
                'lat': 37.4500,
                'lng': -122.1350,
                'infobox': "B"
            }
        ]
    )

    return render_template("events/index.html", map=map)

@events.route("/api/", methods=["GET"])
def events_api():
    j = [{
        "Name" : "North Branch Meeting",
        "Description" : "Description",
        "Location" : "Morlan Centre",
        "Date" : "11/02/18",
        "Time" : "6:30pm",
        "Latitude" : 52.41634,
        "Logitude" : -4.080294
    }]
    return jsonify(j)
