from ceredigionclp import db

class Base(db.Model):
    __abstract__ = True

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    creation_date = db.Column(db.DateTime, default=db.func.current_timestamp())
    modification_date = db.Column(db.DateTime, default=db.func.current_timestamp(), onupdate=db.func.current_timestamp())

class Event(Base):
    __tablename__ = "events"
    title = db.Column(db.String(64), unique=False, nullable=False)
    description = db.Column(db.String(2048), unique=False, nullable=True)
    venue = db.Column(db.String(64), nullable=False)
    date = db.Column(db.DateTime, nullable=False)
    latitude = db.Column(db.Float, nullable=False)
    longitude = db.Column(db.Float, nullable=False)
