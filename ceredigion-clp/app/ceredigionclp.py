from flask import render_template, request, send_from_directory, abort, Flask
from flask_sqlalchemy import SQLAlchemy
from flask_bcrypt import Bcrypt
from flask_login import LoginManager

from flask_googlemaps import GoogleMaps, Map

from config import BaseConfig

app = Flask(__name__)
app.config.from_object(BaseConfig)
db = SQLAlchemy(app)
bcrypt = Bcrypt(app)

maps = GoogleMaps(app)

login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = "admin.login"

from module_admin.controllers import admin
from module_events.controllers import events
from module_minutes.controllers import minutes


@app.route("/")
def index():
    return render_template("index.html")

@app.route("/branches")
def branches():
    return render_template("branches.html")

app.register_blueprint(minutes)
app.register_blueprint(admin)
app.register_blueprint(events)
db.create_all()
