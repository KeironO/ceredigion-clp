from ceredigionclp import db

class Base(db.Model):
    __abstract__ = True

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    creation_date = db.Column(db.DateTime, default=db.func.current_timestamp())

class Minute(Base):
    __tablename__ = "minutes"
    source = db.Column(db.String, nullable=False)
    pdf = db.Column(db.LargeBinary, nullable=False)
    odt = db.Column(db.LargeBinary, nullable=False)
    date = db.Column(db.DateTime, nullable=False)
