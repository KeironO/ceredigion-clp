from flask import Blueprint, jsonify, render_template, flash, g, abort, redirect, url_for

from module_minutes.models import Minute

from ceredigionclp import app, db, login_manager

minutes = Blueprint("minutes", __name__, url_prefix="/minutes")

@minutes.route("/", methods=["GET", "POST"])
def index():
    return render_template("minutes/index.html")
