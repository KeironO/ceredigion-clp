from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand

from ceredigionclp import app, db
from module_admin.models import User

app.config.from_object(app.config)

migrate = Migrate(app, db)
manager = Manager(app)

manager.add_command("db", MigrateCommand)

@manager.command
def create_db():
    db.create_all

@manager.command
def drop_db():
    db.drop_all()


if __name__ == "__main__":
    manager.run()
