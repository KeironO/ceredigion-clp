from flask import Blueprint, request, render_template, flash, g, abort, redirect, url_for

from flask_login import login_user, logout_user, current_user, login_required


from module_admin.models import User
from module_admin.forms import LoginForm

from ceredigionclp import app, db, bcrypt, login_manager

admin = Blueprint("admin", __name__, url_prefix="/admin")

@admin.route("/", methods=["GET", "POST"])
def login():
    form = LoginForm()

    if form.validate_on_submit():
        user = User.query.filter_by(user_name = form.username.data).first()
        if user and user.is_correct_password(form.password.data):
            return "Logged In"
    else:
        flash("Hello World")
    return render_template("admin/login.html", form=form)

@admin.route("/test", methods=["GET", "POST"])
def create_admin():
    user = User(
        user_name = "admin",
        password = "password",
        email_address = "email@email.com"
    )

    db.session.add(user)
    db.session.commit()
    return "Admin Created"
