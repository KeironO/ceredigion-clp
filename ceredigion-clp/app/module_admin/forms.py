from flask_wtf import FlaskForm

from wtforms import StringField, PasswordField
from wtforms.validators import DataRequired

class LoginForm(FlaskForm):
    username = StringField("Username", [DataRequired("You need to provide a username")])
    password = PasswordField("Password", [DataRequired("You need to provide a password")])
